import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  clickMessage = "";

  userTable = [
    {
      id: 1,
      first_name: "Iker",
      last_name: "Landajuela",
      twitter: "@ikernaix"
    },
    {
      id: 2,
      first_name: "Iker_2",
      last_name: "Landajuela_2",
      twitter: "@ikernaix_2"
    }
  ];

  handleClick() {
    console.log("handleClick");
    this.clickMessage = "Has hecho clic en botón";
  }
}
