import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Cliente } from "../modelo/cliente";
import { CLIENTES } from "../mocks/mock-clientes";

@Injectable({
  providedIn: "root"
})
export class ClientesService {
  clientes: Cliente[] = CLIENTES;

  getClientes(): Observable<Cliente[]> {
    return of(CLIENTES);
  }

  getCliente(id: number): Observable<Cliente> {
    return of(this.clientes.find(cliente => cliente.id === id));
  }

  deleteClientes(id: number): Observable<number> {
    //var new_array = arr.filter(callback[, thisArg])
    //   - callback: Función a la que llama para testear array
    this.clientes = this.clientes.filter(cliente => cliente.id !== id);
    return of(id);
  }

  putCliente(cliente: Cliente): Observable<Cliente> {
    const posicionArray = this.clientes.findIndex(
      clienteRepositorio => clienteRepositorio.id === cliente.id
    );

    this.clientes[posicionArray] = cliente;

    return of(cliente);
  }

  postCliente(cliente: Cliente): Observable<Cliente> {
    const nuevoId: number =
      Math.max.apply(Math, this.clientes.map(cliente => cliente.id)) + 1;

    console.log("postCliente: cliente:" + cliente);
    console.log("postCliente: nuevoId:" + nuevoId);

    cliente.id = nuevoId;

    this.clientes.push(cliente);

    return of(cliente);
  }
}
