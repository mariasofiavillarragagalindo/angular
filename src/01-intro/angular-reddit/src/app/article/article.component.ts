import { Component, OnInit } from "@angular/core";

import { Article } from "./article.model";

@Component({
  selector: "app-article",
  templateUrl: "./article.component.html",
  styleUrls: ["./article.component.css"]
})
export class ArticleComponent implements OnInit {
  /*votes: number;
  title: string;
  link: string;*/
  article: Article;

  constructor() {
    /*this.title = "Angular";
    this.link = "http://angular.io";
    this.votes = 10; */
    this.article = new Article("Angular", "http://angular.io", 10);
  }

  voteUp(): boolean {
    //this.votes += 1;
    //this.article.votes += 1;
    //console.log("voteUp, total:" + this.votes);
    this.article.voteUp();
    return false;
  }

  voteDown(): boolean {
    //this.votes -= 1;
    //this.article.votes -= 1;
    this.article.voteDown();
    return false;
  }

  ngOnInit() {}
}
