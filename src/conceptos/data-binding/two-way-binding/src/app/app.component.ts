import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "two-way-binding";

  pelicula;

  productName: string;

  constructor() {
    this.pelicula = { id: 1, titulo: "Aquaman", anio: 2018 };
  }

  /**
   * Método que captura evento cuando cambia input con (ngModelChange)
   * @param valor
   */
  cambiopelicula(valor) {
    console.log("cambio pelicula:" + valor);
  }
}
