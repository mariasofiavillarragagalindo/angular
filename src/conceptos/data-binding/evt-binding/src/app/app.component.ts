import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "evt-binding";
  msg: string = "";
  nombre: string;

  onSubmit() {
    console.log("onSubmit");
    this.msg = "Submit clicked!";
  }

  addData(newtitle: string, newlink: string) {
    console.log("newtitle:" + newtitle + ", newlink:" + newlink);
  }

  onInputNombre(event: Event) {
    console.log(event);
    this.nombre = (<HTMLInputElement>event.target).value;
  }
}
