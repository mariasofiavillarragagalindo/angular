import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <h1>{{ titulo }}</h1>
    <h2>Soy: {{ nombre }}</h2>

    <a href="{{ miHref }}">Enlace</a>

    <p>La suma de 1 + 1 es {{ 1 + 1 }}.</p>

    <p>Llamada a método retorna: {{ 1 + 1 + dameValor() }}.</p>

    <ul>
      <li *ngFor="let cliente of clientes">{{ cliente.nombre }}</li>
    </ul>

    <p>{{ !valorLogico }}</p>

    <!-- Error 
    <p>{{ valorLogico = false }}</p>
    -->

    <!--
    <p>{{ metodoNoRecomendado() }}</p>
    -->

    <button (click)="onClickMe()">Click me!</button>
  `
})
export class AppComponent {
  titulo = "interpolation";
  nombre: string = "Iker";
  miHref = "https://gitlab.com/soka";
  valorLogico = true;

  clientes = [{ id: 1, nombre: "iker" }, { id: 2, nombre: "asier" }];

  dameValor(): number {
    return 2;
  }

  /* 
  metodoNoRecomendado() {
    this.valorLogico = false;
  } 
  */

  /**
   * Chivato para ver estado propiedades componente.
   */
  onClickMe() {
    console.log(this.valorLogico);
    console.log(this.clientes);
    this.nombre = "Soy yo"; // Activamos interpolación a placer
  }
}
