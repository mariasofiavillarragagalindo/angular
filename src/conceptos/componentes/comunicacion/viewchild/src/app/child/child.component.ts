import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-child",
  templateUrl: "./child.component.html",
  styleUrls: ["./child.component.css"]
})
export class ChildComponent implements OnInit {
  message = "Hola Mundo!";

  constructor() {
    console.log("ChildComponent::constructor");
  }

  ngOnInit() {
    console.log("ChildComponent::ngOnInit");
  }
}
