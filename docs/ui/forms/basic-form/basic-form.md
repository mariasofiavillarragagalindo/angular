[TOC]

En el siguiente ejercicio voy a introducir un control de un formulario, un campo de edición de texto `<input>`, para demostrar el _data binding_ a media que se introduzcan carácteres se irá actualizando el mismo contenido en un texto más abajo.

También mostraré como cambiar el contenido del campo de edición desde el componente cuando se pulse un botón.

Efecto final:

![](img/01.gif)

# Registrando el módulo de formularios reactivos

En [src/app/app.module.ts] añadimos la siguiente línea en la cabecera para importar el módulo `ReactiveFormsModule` y poder usar la clase `FormControl`.

```ts
import { ReactiveFormsModule } from "@angular/forms";
```

En la sección `imports` del decorador `@NgModule`:

```ts
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule], //<------
  providers: [],
  bootstrap: [AppComponent]
})
```

# Componente

Enn [src/app/app.component.ts] declaro un atributo de la clase que es una instancia de `FormControl`, usando el parámetro del constructor

```ts
import { Component } from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "basic-form";

  email = new FormControl(""); // <----
}
```

Si quisiésemos definir un texto por defecto cuando se cargue componente podemos pasar la cadena al constructor en lugar de la cadena vacía de arriba.

# El control en la vista

El formulario se define como un grupo de controles. Cada control tendrá un nombre y una configuración. Esa definición permite establecer un valor inicial al control.

En [src/app/app.component.html]:

```ts
<div class="container">
  <label>
    Email:
    <input type="text" [formControl]="email">
  </label>
</div>
```

Usamos el _binding_ unidireccional (_[box]_ del modelo a la vista) con`formControl` para asociar el atributo de la clase con el elemento de la plantilla usando su nombre. 


# Controlando el valor del campo de edición

```ts
<p>Value: {{ email.value }}</p>
```

# Actualizando el valor del formulario

Ahora se trata de cambiar el valor inicial del campo de edición mediante código sin interacción del usuario.

Podemos hacerlo en el propio constructor (sólo lo hace una vez) o de forma dinámica usando el método `.setValue()` de la instancia `FormControl`.

```ts
export class AppComponent {
  title = "basic-form";

  email = new FormControl("ikerlandajuela@gmail.com"); // <----
}
```

Declaro un método que se ejecutará en respuesta al evento de pulsación en un nuevo botón:

```ts
  updateEmail() {
    this.email.setValue("ikernaix@gmail.com");
  }
```

# Data binding bidireccional

Antes de nada para usar la directiva `ngModel` en un formulario es necesario importar la clase `FormsModule` de "@angular/forms" en [src/app/app.module.ts], esta clase exporta las directivas que necesitamos par poder importarlas en `NgModules`.

Para definir el nuevo control `<input>` usaré la directiva `ngModel` con la sintaxis conocida como _banana in a box _  que representa la comunicación bidireccional, _(banana)_ vista a modelo y _[box]_ del modelo a la vista.

```ts
  <section>
    <label for="name">
      Name:
      <input
        name="name"
        type="text"
        [(ngModel)]="name"
        placeholder="Contact name"
      />
    </label>
  </section>
```

En el componente he sobrecargado el descriptor de acceso `set` para ir monitorizando por consola como recibe las cadenas de texto, cada carácter introducido envía todo el contenido de campo de edición, no sólo el último carácter. Igualmente como en el ejemplo anterior puedo definir el contenido desde TypeScript.

```ts
export class AppComponent {
  title = "basic-form";
  email = new FormControl("ikerlandajuela@gmail.com"); // <----

  _name: string;

  get name(): string {
    return this._name;
  }

  set name(newName: string) {
    console.log(newName);
    this._name = newName;
  }

  updateEmail() {
    this.email.setValue("ikernaix@gmail.com");
    this._name = "Iker";
  }
}
```

![](img/02.gif)

# Código fuente del proyecto

- angular / src / ui / forms / [basic-form](https://gitlab.com/soka/angular/tree/master/src/ui/forms/basic-form).


# Enlaces externos

- ["Reactive Forms - Angular"](https://angular.io/guide/reactive-forms).
- ["Crear un Formulario con Angular 7 y Bootstrap 4 + Validación – Parte 2 (Final)"](http://blog.nubecolectiva.com/crear-un-formulario-con-angular-7-y-bootstrap-4-validacion-parte-2-final/).
- ["Validación de formulario Angular con formularios reactivos y controlados por plantillas"](https://code.tutsplus.com/es/tutorials/angular-form-validation-with-reactive-and-template-driven-forms--cms-32131).
- ["Angular 7 Forms Tutorial Example - AppDividend"](https://appdividend.com/2018/10/24/angular-7-forms-tutorial-example/).
- Academia Binaria ["Formularios reactivos con Angular"](https://academia-binaria.com/formularios-reactivos-con-Angular/).
- Academia Binaria ["Formularios, tablas y modelos de datos en Angular"](https://academia-binaria.com/formularios-tablas-y-modelos-de-datos-en-angular/).
