[TOC]

# Introducción

[JSON Server](https://github.com/typicode/json-server) es una API REST NodeJS para entornos de desarrollo y pruebas. La instalación es muy sencilla usando NPM:

```js
npm install -g json-server
```

Una vez instalado creamos un fichero db.json con los datos, por ejemplo:

```json
{
  "posts": [{ "id": 1, "title": "json-server", "author": "typicode" }],
  "comments": [{ "id": 1, "body": "some comment", "postId": 1 }],
  "profile": { "name": "typicode" }
}
```

Arrancamos el servidor:

```js
json-server --watch db.json
```

Ahora ya podemos abrir [http://localhost:3000/posts/1](http://localhost:3000/posts/1) y nos devuelve:

```json
{ "id": 1, "title": "json-server", "author": "typicode" }
```

Podemos obtener un registro por su ID por ejemplo [http://localhost:3000/comments/1](http://localhost:3000/comments/1).

Las operaciones POST, PUT, PATCH y DELETE modifican el fichero db.json (usando [lowdb](https://github.com/typicode/lowdb)).

Para probar el resto de operaciones yo recomiendo [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) para Chrome.
