[TOC]

A pesar de que **Angular CLI** nos proporciona un comando para generar el esqueleto básico de un componente en esta ocasión vamos a comenzar creándolo desde cero.

Dentro de la carpeta [src/app] creamos una carpeta "personas" que contendrá el componente para listar una serie de personas. Dentro creamos un fichero vacío "personas.component.ts". No es obligatorio que el nombre del fichero tenga esta estructura pero es una convención que se componga de un nombre, el tipo de información que contiene (un _component_) y la extensión .ts de TypeScript (las plantillas tendrán extensión .html y los estilos .css).

# Creación de un componente básico

Un componente es simplemente una clase TypeScript, añadimos la palabra reservada `export` para la clase sea visible desde otros archivos del proyecto.

```ts
export class PersonasComponent {}
```

Para convertir esta clase en un componente necesitamos añadir un decorador con ciertas características.

Añadimos la importación del decorador en la cabecera del fichero:

```ts
import { Component } from "@angular/core";
```

Ahora defino los elementos del decorador:

```ts
import { Component } from "@angular/core";

@Component({
  selector: "app-personas",
  templateUrl: "./personas.component.html"
})
export class PersonasComponent {}
```

Primero he definido el nombre del componente (debe ser único), a continuación he creado el fichero con la plantilla visual.

```ts
<h1>Listado de personas</h1>
```

# Registro del componente

Para poder usar el nuevo componente debemos registrarlo en [src/app/app.module.ts]:

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { PersonasComponent } from "./personas/personas.component"; // <----

@NgModule({
  declarations: [AppComponent, PersonasComponent], // <----
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

# Uso del componente

Debemos añadir del componente dentro de "app.component.html":

```ts
<h1>Welcome to {{ title }}!</h1>
<app-personas></app-personas>
```

![](img/01.PNG)

# Código fuente de ejemplo

- angular / src / conceptos / componentes / crear-componte-forma-manual / [componente-manual](https://gitlab.com/soka/angular/tree/master/src/conceptos/componentes/crear-componte-forma-manual/componente-manual).
