[TOC]

El siguiente ejercicio consta de un formulario con dos campos _input_ para introducir dos operandos y un botón que realiza la suma y se muestra el resultado.

Técnicas tratadas en este ejercicio práctico:

- Data binding bidireccional con `[(ngModel)]`.
- Data binding unidireccional del modelo a la vista con "{{}}".
- Event binding con un botón del formulario.

# Creación del proyecto

```ts
ng new ejercicio-calculadora-sum
```

# Agregamos soporte two way binding

Para poder usar la comunicación bidireccional lo primero es importar [`FormsModule`](https://angular.io/api/forms/FormsModule) de [`@angular/forms`](https://angular.io/api/forms). He marcado con un comentario las modificaciones que he realizado en [src/app/app.module.ts]:

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { FormsModule } from "@angular/forms"; //<---

import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, FormsModule], //<---
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

# Definición de datos y métodos en el componente

En la clase `AppComponent` he definido nuevos atributos para los operandos de la suma y el resultado. También he definido un método que no retorna nada y que suma los operandos y guarda en resultado:

```ts
  operandoA: number;
  operandoB: number;
  resultado: number;

  onSuma(): void {
    this.resultado = this.operandoA + this.operandoB;
  }
```

# Bootstrap para dar estilo a la plantilla

En el raíz del proyecto ejecutamos:

```ts
npm install bootstrap jquery popper.js
```

Si todo ha ido correctamente deberíamos ver esta nueva carpeta en nuestro proyecto [node_modules\bootstrap]

Ahora debemos incluir los nuevos estilos dentro del fichero "angular.json" para que nuestra aplicación reconozca los cambios que hemos realizado:

```ts
            "styles": [
              "node_modules/bootstrap/dist/css/bootstrap.css",
              "src/styles.css"
            ],
```

En el apartado de scripts agregamos también Bootstrap y las dependencias que hemos instalado al inicio.

```ts
            "scripts": [
              "node_modules/jquery/dist/jquery.slim.min.js",
              "node_modules/popper.js/dist/umd/popper.min.js",
              "node_modules/bootstrap/dist/js/bootstrap.min.js"
            ],
```

**NOTA**: Para que los cambios tomen efecto será necesario parar y volver a lanzar el servidor con `ng serve -o`.

# Plantilla

Ahora diseño la vista usando los estilos de [Bootstrap](https://getbootstrap.com/docs/4.0/components/forms/) para el formulario que contendrá dos etiquetas `<input>` y un `<button>`. El resultado de la suma se mostrará a continuación del formulario.

Cada entrada la envuelvo en una capa `<div>` definida con el atributo de la clase [`form-group`](https://getbootstrap.com/docs/4.0/components/forms/#form-groups) para agrupar la etiqueta `<label>` y el propio control _input_ de edición que usa la clase [`form-control`](https://getbootstrap.com/docs/4.0/components/forms/#form-controls).

![](img/02.PNG)

Para dar un estilo básico al [botón con Bootstrap](https://getbootstrap.com/docs/4.0/components/buttons/) defino la clase con `class="btn btn-primary"`.

```ts
<div class="container">
  <div style="text-align:center">
    <h1>Aplicación: {{ titulo }}</h1>
  </div>

  <form>
    <div class="form-group">
      <label for="operandoA">OperandoA:</label>
      <input
        type="number"
        class="form-control"
        id="operandoA"
        name="operandoA"
        placeholder="Escribe operando A"
        [(ngModel)]="operandoA"
      />
    </div>

    <div class="form-group">
      <label for="operandoB">OperandoB:</label>
      <input
        type="number"
        class="form-control"
        id="operandoB"
        name="operandoB"
        placeholder="Escribe operando B"
        [(ngModel)]="operandoB"
      />
    </div>

    <button type="submit" class="btn btn-primary" (click)="onSumar()">
      Sumar
    </button>
  </form>
  <p>Resultado {{ resultado }}</p>
</div>
```

Mi calculadora en acción:

![](img/04.gif)

# Proyecto en GitLab

- angular / src / conceptos / data-binding / [ejercicio-calculadora-sum](https://gitlab.com/soka/angular/tree/master/src/conceptos/data-binding/ejercicio-calculadora-sum).

# Proyecto en Stackblitz

<iframe width="100%" height="600px" src="https://stackblitz.com/edit/calculadora-app?embed=1&file=src/app/app.component.ts"></iframe>
