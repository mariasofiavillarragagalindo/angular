[TOC]

![](img/01.png)

**Interpolación: Modelo a vista**

- [Interpolación](interpolacion/interpolacion.md): Del componente al DOM.
- [Ejercicio básico](string-interpolation-personas/string-interpolation-personas.md): Ejemplo básico (mostrar datos de una persona).

**Enlazado de propiedades: Modelo a vista**

- [Enlazado de propiedades](property-binding/property-binding.md).

**Event binding**

- [Event binding](event-binding/event-binding.md)

**Two way binding**

- [Two way binding](two-way-binding/two-way-binding.md).

**Ejercicio: Calculadora para sumar dos operandos**

- [Ejercicio calculadora](ejercicio-calculadora-suma/ejercicio-calculadora-suma.md)
