[TOC]

**NOTA**: Para poder usarl `ngModel` es necesario importar `FormsModule` de "@angular/forms" en app.module.ts.

# Comunicación bidireccional entre vista y modelo

La directiva **ngModel**, comunica un modelo con un formulario, es especial porque **permite comunicarlos de forma bidireccional**.

Ejemplo básico, declaro un atributo de tipo cadena de texto para guardar el nombre de un producto:

```ts
productName: string;
```

Diseño la vista con una etiqueta y un control de tipo campo de edición, uso la directiva `ngModel` con la sintaxis _banana in a box_ `[(...)]` (esta notación es lo que llaman _syntactic sugar_ o azucarillo sintáctico)

```ts
<label for="productNameInput">Product Name</label>
<input
  type="text"
  id="productNameInput"
  placeholder="Product Name"
  name="productName"
  [(ngModel)]="productName"
/>
```

Si queremos ver como actúa podemos añadir la siguiente línea en la vista:

```ts
<p>{{ productName }}</p>
```

Este es el efecto:

![](img/01.gif)

# Evento ngModelChange

En el siguiente ejemplo establezco un valor inicial para el campo de entrada aprovechando e constructor de la clase:

```ts
export class AppComponent {
  title = "two-way-binding";

  pelicula;

  constructor() {
    this.pelicula = { id: 1, titulo: "Aquaman", anio: 2018 };
  }

  cambiopelicula(valor) {
    console.log("cambio pelicula:" + valor);
  }
}
```

En la vista añado `(ngModelChange)="cambiopelicula($event)"` al control para gestionar cualquier cambio en el control.

```ts
<input
  type="text"
  [(ngModel)]="pelicula.titulo"
  (ngModelChange)="cambiopelicula($event)"
/>
<p>Mostrar titulo: {{ pelicula.titulo }}</p>
```

En acción:

![](img/02.gif)

# Código fuente

- GitLab: angular / src / conceptos / data-binding / [two-way-binding](https://gitlab.com/soka/angular/tree/master/src/conceptos/data-binding/two-way-binding).

<iframe width="100%" height="600px" src="https://stackblitz.com/edit/two-way-bind?embed=1&file=src/app/app.component.ts"></iframe>

# Enlaces externos

- API > @angular/forms [**NgModel**](https://angular.io/api/forms/NgModel) Directive. Con buenos ejemplos y profusamente documentado.
