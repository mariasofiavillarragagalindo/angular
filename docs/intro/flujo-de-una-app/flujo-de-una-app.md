[TOC]

En este tutorial vamos a revisar como se ejecuta una aplicación en **Angular**. index.html será nuestra página SPA. Dentro tenemos el elemento `<app-root></app-root>` que se define como componente dentro de /src/app/app.component.ts. Un componente es una clase estándar de ES6 decorada con `@Component`.

```JS
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  titulo = "mi primera app en Angular";
}
```

El `selector` tiene el mismo nombre que la etiqueta en [src/index.html] `<app-root></app-root>`.

En [src/main.ts] se puede observar que estamos levantando el módulo de `AppModule`:

```JS
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
```

Este modulo es una clase de **TypeScript**. Si vamos a [/src/app/app.module.ts] podemos observar que contiene una clase `AppModule`.

```JS
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

El decorador `NgModule` lo importamos del _core_ de **Angular** y esá compuesto por varios apartados, _bootstrap_ se refiere al componente que vamos a llamar durante el arranque de nuestra aplicación (`AppComponent`).

Ahora ya podemos entender un poco mejor que es un componente, es una clase TypeScript, también contiene un decorador con varios elementos, en el ejemplo básico contiene el selector que referenciamos para inscrustarlo en index.html (podemos usarlo en cualquier otra plantilla), a continuación definimos la plantilla HTML que vamos a utilizar, en último lugar indicamos el archivo CSS para embellecer y dar estilo a nuestra plantilla.

```ts
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  titulo = "mi primera app en Angular";
}
```

# Enlaces externos

- ["Intro a Angular (parte I): Modulo, Componente, Template y Metadatos"](http://blog.enriqueoriol.com/2017/03/introduccion-angular-modulo-y-componente.html#componente).
- ["Introducción a Angular - Desarrolloweb.com"](https://desarrolloweb.com/articulos/introduccion-angular2.html).
- ["Introducción a Angular JS para Principiantes - Guiadev"](https://guiadev.com/introduccion-angular-js/).
